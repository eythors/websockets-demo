Short demo showcasing websockets in python and javascript. No frills.

To get started, run the following:

```
$ nix develop
$ poetry run python -m demo
```

Then open `index.html` in your browser.
