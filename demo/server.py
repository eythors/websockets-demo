import websockets
import asyncio
import json
from datetime import datetime
import random

def current_state():
    return json.dumps({
        "time": datetime.now().isoformat(),
        "state": random.choice(["idle","measuring","homing","heating","display","pulsa"]),
        "battery": round(random.random(), 1)
        })

async def echo(websocket, path):
    async for message in websocket:
        print(f"Received message: {message}")
        await websocket.send(current_state())

async def main():
    async with websockets.serve(echo, "localhost", 8765):
        print("Server started on ws://localhost:8765")
        await asyncio.Future()  # Run forever


